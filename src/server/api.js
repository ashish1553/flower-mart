// Simple Express server setup to serve for local testing/dev API server
const compression = require('compression');
const helmet = require('helmet');
const express = require('express');
require('dotenv').config()
const path = require('path');
const { Client } = require('pg');
const jsforce = require('jsforce');
require('dotenv').config();
const cors = require('cors');

const app = express();
app.use(helmet.contentSecurityPolicy({
    directives: {
        defaultSrc: ["'self'"],
        scriptSrcElem: ["'self'", "unsafe-inline"],
        styleSrc: ["'self'", "unsafe-inline"],
        imgSrc: ["'self'", "*.imgur.com"]
    }
}))
app.use(cors());
app.use(express.json());
app.use(compression());

/* ---------------------------------------------------- */

// const HOST = process.env.API_HOST || 'localhost';
// const PORT = process.env.API_PORT || 3002;

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3001;
const DIST_DIR = './dist';

app.use(express.static(DIST_DIR));

app.use(/^(?!\/api).+/, (req, res) => {
    res.sendFile(path.resolve(DIST_DIR, 'index.html'));
});

/* ----------------------------------------------------- */

const { SF_LOGIN_URL, SF_USERNAME, SF_PASSWORD, SF_TOKEN } = process.env;

if (!(SF_USERNAME && SF_PASSWORD && SF_TOKEN && SF_LOGIN_URL)) {
    console.error(
        'Cannot start app: missing mandatory configuration. Check your .env file.'
    );
    process.exit(-1);
}

const conn = new jsforce.Connection({
    loginUrl: SF_LOGIN_URL
})

conn.login(SF_USERNAME, SF_PASSWORD + SF_TOKEN, (err, userInfo) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log("User Id: ", userInfo.id);
        console.log("Org Id: ", userInfo.organizationId);
        console.log("Url: ", userInfo.url);
    }
});



app.get('/api/data/plants', (req, res) => {
    var plants = [];
    const client = new Client({
        connectionString: process.env.DATABASE_URL,
        ssl: {
            rejectUnauthorized: false
        }
    });
    client.connect();
    client.query('SELECT Id, Name, Date_of_Plantation__c, Description__c, Image_Url__c, Price__c, Rating__c, Discount__c FROM salesforce.Plant__c; ', (err, result) => {
        if (err) {
            console.log('error occour');
            console.log(err);
        } else {
            // console.log(result.rows);
            plants = result.rows.map(plantRecord => {
                console.log(plantRecord.name);
                return {
                    Id: plantRecord.id,
                    Name: plantRecord.name,
                    Date_of_Plantation__c: plantRecord.date_of_plantation__c,
                    Description__c: plantRecord.description__c,
                    Image_Url__c: plantRecord.image_url__c,
                    Discount__c: plantRecord.discount__c,
                    Rating__c: plantRecord.rating__c,
                    Price__c: plantRecord.price__c
                }
            })
            res.send({ data: plants });
        }
        client.end();
    });

})

app.use((req, res, next) => {
    res.setHeader(
        'Content-Security-Policy',
        "default-src 'self'",
        "img-src 'self'",
        "script-src 'self' http://localhost:3001"
    )
    next()
})

app.get('/api/v1/endpoint', (req, res) => {
    res.json({ success: true });
});

app.listen(PORT, () =>
    console.log(
        `✅  API Server started: http://${HOST}:${PORT}`
    )
);

app.post('/api/post/plantOrder', (req, res) => {
    var reqBody;
    var stringifiedBody = JSON.stringify(req.body);
    stringifiedBody = stringifiedBody.replace("Name", "name");
    stringifiedBody = stringifiedBody.replace("Description__c", "description");
    stringifiedBody = stringifiedBody.replace("Image_Url__c", "imageUrl");
    stringifiedBody = stringifiedBody.replace("Rating__c", "rating");
    stringifiedBody = stringifiedBody.replace("Date_of_Plantation__c", "dateOfPlantation");
    stringifiedBody = stringifiedBody.replace("Discount__c", "discount");
    stringifiedBody = stringifiedBody.replace("Price__c", "price");

    reqBody = { formDetails: stringifiedBody };
    console.log("req.body :", reqBody);

    conn.apex.post("/placeOrder/", reqBody, function (err, response) {
        if (err) {
            console.error(err);
        } else {
            res.json({ success: true })
        }
    });
});
