import { LightningElement } from 'lwc';
import { getPlants } from 'data/plantData';
export default class Home extends LightningElement {

    plants;
    clickedPlantId;
    connectedCallback() {
        getPlants().then(result => {
            this.plants = result;
            console.log(result);
        });

    }
}
