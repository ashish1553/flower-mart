import { LightningElement, wire, api } from 'lwc';
import { routeParams } from "@lwce/router"
export default class PlantDetail extends LightningElement {

    @wire(routeParams) params;

    @api plants;

    showForm = false;

    selectedPlant;

    orderDetails = {
        nameOfCustomer: '',
        emailAddress: '',
        address: {
            street: '',
            city: '',
            province: '',
            country: '',
            postalCode: '',
            validity: ''
        },
        selectedPlant: ''
    }

    connectedCallback() {
        this.selectedPlant = this.plants.find(plant => {
            return plant.Id == this.params.clickedPlantId;
        });
    }

    get stars() {
        var obj = [];
        for (let i = 1; i <= 5; i++) {
            if (this.selectedPlant.Rating__c >= i) {
                obj.push({ id: i, show: true });
            } else {
                obj.push({ id: i, show: false });
            }
        }
        return obj;
    }

    get plantDiscount() {
        return 'Save ' + this.selectedPlant.Discount__c + '%';
    }

    handleBuyNow() {
        this.showForm = true;
    }

    getCustomerName(event) {
        this.orderDetails.nameOfCustomer = event.detail.value;
    }

    getCustomerEmail(event) {
        this.orderDetails.emailAddress = event.detail.value;
    }

    getCustomerAddress(event) {
        this.orderDetails.address.street = event.detail.street;
        this.orderDetails.address.country = event.detail.country;
        this.orderDetails.address.city = event.detail.city;
        this.orderDetails.address.postalCode = event.detail.postalCode;
        this.orderDetails.address.province = event.detail.province;
        this.orderDetails.address.validity = event.detail.validity;
    }

    handleOrder() {
        this.orderDetails.selectedPlant = this.selectedPlant;
        console.log(this.orderDetails);
        this.showSpinner = true;
        fetch('/api/post/plantOrder', {
            method: 'POST',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.orderDetails)
        }).then(response => {
            if (!response.ok) {
                throw new Error('No Response From Server');
            }
            return response.json();
        }).then(result => {
            console.log("Order Placed: ", result);
            location.assign("/");
            // this.showSpinner = false;
            // this.dispatchEvent(new CustomEvent('pageswitch', {
            //     detail: 'accountsDatatable'
            // }));
        }).catch(error => {
            console.error(error);
        })
    }
}