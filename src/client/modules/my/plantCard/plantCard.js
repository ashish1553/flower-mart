import { LightningElement, api } from 'lwc';
export default class PlantCard extends LightningElement {

    @api plant;

    get plantDiscount() {
        return 'Save ' + this.plant.Discount__c + '%';
    }

    get stars() {
        var obj = [];
        for (let i = 1; i <= 5; i++) {
            if (this.plant.Rating__c >= i) {
                obj.push({ id: i, show: true });
            } else {
                obj.push({ id: i, show: false });
            }
        }
        return obj;
    }

    get plantLink() {
        return '/plantdetail/' + this.plant.Id;
    }
}