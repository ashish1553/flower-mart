const URL = '/api/data/plants';
let plants = [];
export const getPlants = () => fetch(URL)
    .then(response => {
        if (!response.ok) {
            throw new Error('No response from server');
        }
        return response.json();
    })
    .then(result => {
        plants = result.data;
        return plants;
    });